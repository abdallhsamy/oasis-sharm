<?php

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['namespace' => 'Api\V1', 'prefix' => 'v1'], function () {

    Route::get('global-roles', function () {
        return \Spatie\Permission\Models\Role::get(['id', 'name']);
    });

    Route::post('login',                    'Auth\AuthApiController@login')->name('login');

    Route::post('password/create',          'Auth\PasswordResetController@create');
    Route::get('password/find/{token}',     'Auth\PasswordResetController@find');
    Route::post('password/reset',           'Auth\PasswordResetController@reset');

    Route::group(['middleware' => 'auth:api'], function () {

        Route::get('details',               'Auth\AuthApiController@user');
        Route::post('logout',               'Auth\AuthApiController@logout');

        Route::get('vehicle-configs/create',            'VehicleConfigApiController@create');
        Route::get('bank-accounts/create',              'BankAccountApiController@create');
        Route::get('transactions/create',               'TransactionApiController@create');
        Route::get('transports/create',                 'TransportApiController@create');
        Route::get('excursions/create',                 'ExcursionApiController@create');
        Route::get('transports/find',                   'TransportApiController@find');
        Route::get('vouchers/create',                   'VoucherApiController@create');
        Route::get('vehicles/create',                   'VehicleApiController@create');
        Route::get('rooms/create',                      'RoomApiController@create');
        Route::get('users/create',                      'UserApiController@create');


        Route::apiResource('companies',                 'CompanyApiController');
        Route::apiResource('payees',                    'PayeeApiController');
        Route::apiResource('transactions',              'TransactionApiController');
        Route::apiResource('trips',                     'TripApiController');
        Route::apiResource('drivers',                   'DriverApiController');
        Route::apiResource('vouchers',                  'VoucherApiController');

        Route::apiResource('hotels',                    'HotelApiController');
        Route::apiResource('rooms',                     'RoomApiController');

        Route::apiResource('banks',                     'BankApiController');
        Route::apiResource('bank-accounts',             'BankAccountApiController');

        Route::apiResource('excursions',                'ExcursionApiController');
        Route::apiResource('excursion-vehicles',        'ExcursionVehicleApiController');
        Route::apiResource('excursion-vehicle-types',   'ExcursionVehicleTypeApiController');

        Route::apiResource('vehicles',                  'VehicleApiController');
        Route::apiResource('vehicle-types',             'VehicleTypeApiController');

        Route::apiResource('transports',                'TransportApiController');
        Route::apiResource('transport-amounts',         'TransportAmountApiController');
        Route::apiResource('transport-amount-prices',   'TransportAmountPriceApiController');

        Route::apiResource('vehicle-configs',           'VehicleConfigApiController');
        Route::apiResource('scooter-prices',            'ScooterPriceApiController');

        Route::apiResource('users',                     'UserApiController');
        Route::apiResource('roles',                     'Auth\RoleApiController');
        Route::apiResource('permissions',               'Auth\PermissionApiController');

    });
});
