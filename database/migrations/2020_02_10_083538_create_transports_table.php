<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->unsignedBigInteger('vehicle_id');
            $table->unsignedBigInteger('vehicle_config_id');
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->text('notice')->nullable();
            $table->decimal('commission', 25,2)->nullable();
            $table->decimal('plus', 25,2)->nullable();
//            $table->decimal('value', 25,2)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transports');
    }
}
