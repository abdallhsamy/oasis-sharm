<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcursionScooterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excursion_scooter', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('excursion_id');
            $table->unsignedBigInteger('scooter_id');
            $table->unsignedBigInteger('type_id');

            // $table->primary(['excursion_id', 'scooter_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excursion_scooter');
    }
}
