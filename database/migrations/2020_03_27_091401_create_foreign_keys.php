<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('excursions', function (Blueprint $table) {
            $table->foreign('voucher_id')->references('id')->on('vouchers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('transports', function (Blueprint $table) {
            $table->foreign('vehicle_id')->references('id')->on('vehicles')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('vehicles', function (Blueprint $table) {
            $table->foreign('type_id')->references('id')->on('vehicle_types')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade')->onUpdate('cascade');
            // $table->foreign('licence_id')->references('id')->on('licences')->onDelete('cascade')->onUpdate('cascade');
        });
//        Schema::table('transport_amount_prices', function (Blueprint $table) {
//            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_types')->onDelete('cascade')->onUpdate('cascade');
//        });
//        Schema::table('transport_amounts', function (Blueprint $table) {
//            $table->foreign('transport_amount_price_id')->references('id')->on('transport_amount_prices')->onDelete('cascade')->onUpdate('cascade');
//        });
//        Schema::table('transport_amounts', function (Blueprint $table) {
//            $table->foreign('transport_id')->references('id')->on('transports')->onDelete('cascade')->onUpdate('cascade');
//        });
        Schema::table('bank_accounts', function (Blueprint $table) {
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->foreign('payee_id')->references('id')->on('payees')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('vouchers', function (Blueprint $table) {
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade')->onUpdate('cascade');
        });
        Schema::table('scooter_type', function (Blueprint $table) {
            $table->foreign('scooter_id')->references('id')->on('scooters')->onDelete('cascade');
            $table->foreign('scooter_type_id')->references('id')->on('scooter_types')->onDelete('cascade');
        });

        Schema::table('excursion_scooter', function (Blueprint $table) {
            $table->foreign('excursion_id')->references('id')->on('excursions')->onDelete('cascade');
            $table->foreign('scooter_id')->references('id')->on('scooters')->onDelete('cascade');
            // $table->foreign('scooter_type_id')->references('id')->on('scooter_type')->onDelete('cascade');
        });

        Schema::table('vehicle_configs', function (Blueprint $table) {
            $table->foreign('vehicle_type_id')->references('id')->on('vehicle_types')->onDelete('cascade');
            $table->foreign('vehicle_trip_type_id')->references('id')->on('vehicle_trip_types')->onDelete('cascade');
        });

//        Schema::table('transport_vehicle_config', function (Blueprint $table) {
//            $table->foreign('vehicle_config_id')->references('id')->on('vehicle_configs')->onDelete('cascade');
//            $table->foreign('transport_id')->references('id')->on('transports')->onDelete('cascade');
//        });

        Schema::table('transport_details', function (Blueprint $table) {
            $table->foreign('transport_id')->references('id')->on('transports')->onDelete('cascade');
            $table->foreign('vehicle_trip_type_id')->references('id')->on('vehicle_trip_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('excursions', function (Blueprint $table) {
            $table->dropForeign('excursions_voucher_id_foreign');
            $table->dropForeign('excursions_room_id_foreign');
            $table->dropForeign('excursions_trip_id_foreign');
        });
        Schema::table('transports', function (Blueprint $table) {
            $table->dropForeign('transports_vehicle_id_foreign');
            $table->dropForeign('transports_driver_id_foreign');
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->dropForeign('rooms_hotel_id_foreign');
        });
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropForeign('vehicles_type_id_foreign');
            $table->dropForeign('vehicles_driver_id_foreign');
            $table->dropForeign('vehicles_licence_id_foreign');
        });
//        Schema::table('transport_amount_prices', function (Blueprint $table) {
//            $table->dropForeign('transport_amount_prices_vehicle_type_id_foreign');
//        });
//        Schema::table('transport_amounts', function (Blueprint $table) {
//            $table->dropForeign('transport_amounts_transport_amount_price_id_foreign');
//            $table->dropForeign('transport_amounts_transport_id_foreign');
//        });
//        Schema::table('transport_amounts', function (Blueprint $table) {
//        });
        Schema::table('bank_accounts', function (Blueprint $table) {
            $table->dropForeign('bank_accounts_bank_id_foreign');
        });
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign('transactions_bank_account_id_foreign');
            $table->dropForeign('transactions_payee_id_foreign');
        });
        // Schema::table('transactions', function (Blueprint $table) {
        // });
        Schema::table('vouchers', function (Blueprint $table) {
            $table->dropForeign('vouchers_company_id_foreign');
        });
        Schema::table('scooter_type', function (Blueprint $table) {
            $table->dropForeign('scooter_type_scooter_id_foreign');
            $table->dropForeign('scooter_type_scooter_type_id_foreign');
        });
        Schema::table('excursion_scooter', function (Blueprint $table) {
            $table->dropForeign('excursion_scooter_excursion_id_foreign');
            $table->dropForeign('excursion_scooter_scooter_id_foreign');
            // $table->dropForeign('excursion_scooter_scooter_type_id_foreign');
        });
        Schema::table('vehicle_configs', function (Blueprint $table) {
            $table->dropForeign('vehicle_configs_vehicle_type_id_foreign');
            $table->dropForeign('vehicle_configs_vehicle_trip_type_id_foreign');
        });

//        Schema::table('transport_vehicle_config', function (Blueprint $table) {
//            $table->dropForeign('transport_vehicle_vehicle_config_id_foreign');
//            $table->dropForeign('transport_vehicle_transport_id_foreign');
//        });

        Schema::table('transport_details', function (Blueprint $table) {
            $table->dropForeign('transport_details_transport_id_foreign');
            $table->dropForeign('transport_details_vehicle_trip_type_id_foreign');
        });
    }
}
