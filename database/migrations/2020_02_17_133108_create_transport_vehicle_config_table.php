<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportVehicleConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transport_vehicle_config', function (Blueprint $table) {
            $table->unsignedBigInteger('vehicle_config_id');
            $table->unsignedBigInteger('transport_id');
            $table->primary(['vehicle_config_id', 'transport_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transport_vehicle_config');
    }
}
