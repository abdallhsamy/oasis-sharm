<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScooterPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scooter_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('scooter_id');
            $table->unsignedBigInteger('scooter_type_id');
            $table->unsignedBigInteger('trip_id');
            $table->decimal('price', 10, 2);
            $table->timestamps();


            $table->foreign('scooter_id')->references('id')->on('scooters')->onDelete('cascade');
            $table->foreign('scooter_type_id')->references('id')->on('scooter_types')->onDelete('cascade');
            $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scooter_prices');
    }
}
