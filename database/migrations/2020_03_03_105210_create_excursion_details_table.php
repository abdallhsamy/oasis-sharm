<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExcursionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('excursion_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('excursion_id');
            $table->unsignedBigInteger('scooter_id');
            $table->unsignedBigInteger('scooter_type_id');
            $table->unsignedInteger('count')->default(1);
            $table->decimal('price', 10,2);

            $table->foreign('excursion_id')->references('id')->on('excursions')->onDelete('cascade');
            $table->foreign('scooter_id')->references('id')->on('scooters')->onDelete('cascade');
            $table->foreign('scooter_type_id')->references('id')->on('scooter_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('excursion_details');
    }
}
