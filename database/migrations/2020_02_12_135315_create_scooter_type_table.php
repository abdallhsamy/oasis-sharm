<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScooterTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scooter_type', function (Blueprint $table) {
           $table->bigIncrements('id');
//            $table->timestamps();
            $table->unsignedBigInteger('scooter_id');
            $table->unsignedBigInteger('scooter_type_id');
            // $table->primary(['scooter_id','scooter_type_id']);

          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scooter_type');
    }
}
