<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            UserSeeder::class,
            HotelSeeder::class,
            TripSeeder::class,
            ScooterSeeder::class,
            VehicleTypeSeeder::class,
            VehicleConfigSeeder::class,
            OauthClientsTableSeeder::class,
            PermissionSeeder::class,
            ScooterPriceSeeder::class
        ]);
    }
}
