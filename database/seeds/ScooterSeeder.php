<?php

use App\Models\Scooter;
use App\Models\ScooterType;
use Illuminate\Database\Seeder;

class ScooterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scooters = [
            'Quad'  =>  ['Single', 'Double'],
            'Buggy'  =>  ['Single', 'Double', '4x4']
        ];

        foreach ($scooters as $scooter=>$types) {
            $scooter = Scooter::firstOrCreate(['name' => $scooter]);
            foreach ($types as $type) {
                $scooterType = ScooterType::firstOrCreate(['name' => $type]);
                DB::table('scooter_type')->insert([
                    'scooter_id' => $scooter->id,
                    'scooter_type_id' => $scooterType->id,
                ]);
            }
        }
    }
}
