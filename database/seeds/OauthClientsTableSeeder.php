<?php

use App\Models\School\EduClass;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OauthClientsTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('oauth_clients')->truncate();
        DB::table('oauth_clients')->truncate();

        DB::table('oauth_clients')->insert([
            'id' => 1,
            'name' => 'Laravel Personal Access Client',
            'secret' => 'j1Nw7jZqC9U00Zl1KmQZFaTZgkdccKByzq2ELJU2',
            'redirect' => 'http://localhost',
            'personal_access_client' => 1,
            'password_client' => 0,
            'revoked' => 0,
        ]);
        DB::table('oauth_clients')->insert([
            'id' => 2,
            'name' => 'Laravel Password Grant Client',
            'secret' => 'J2WaABiNegYCaDayfQjRpz5bfru4SabjOB38et5b',
            'redirect' => 'http://localhost',
            'personal_access_client' => 0,
            'password_client' => 1,
            'revoked' => 0,
        ]);
    }
}
