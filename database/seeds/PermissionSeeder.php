<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = app_path() . DIRECTORY_SEPARATOR . 'Models';
        $operations = ['view', 'create', 'edit', 'delete'];
        $models = $this->getModels($path);
        
        $admin = Role::firstOrCreate(['name' => 'admin']);
        
        foreach ($models as $model) {
            $model = Str::plural(str_replace('_', ' ', Str::snake($model)));
            foreach ($operations as $operation) {
                $permission = Permission::firstOrCreate(['name' => $operation . ' ' . $model]);
                $admin->givePermissionTo($permission);
            }
        }

        

    }

    private function getModels($path)
    {
        $out = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..') {
                continue;
            }

            $filename = $path . DIRECTORY_SEPARATOR . $result;
            if (is_dir($filename)) {
                $out = array_merge($out, $this->getModels($filename));
            } else {
                $model = explode(DIRECTORY_SEPARATOR, substr($filename, 0, -4));
                $out[] = end($model);
            }
        }
        return $out;
    }
}
