<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'name'  => 'Admin',
            'email' => 'admin@admin.com',
            'password'  => '$2y$10$6o5M80c15xl1QNBTIKnNMexxPCMXCC6QP97IT7EVDqjXhgqUIFx2u',
        ]);
    }
}
