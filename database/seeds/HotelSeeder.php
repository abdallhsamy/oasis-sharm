<?php

use App\Models\Hotel;
use Illuminate\Database\Seeder;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hotels = [
            "AIDA SHARM"
            ,"ALBATROS AQUA BLU"
            ,"AMWAJ"
            ,"AQUA BLU"
            ,"AQUA BLUE"
            ,"AQUA PARK"
            ,"BARCELO"
            ,"BLUE BAY"
            ,"CATARACT"
            ,"CLEOPATRA"
            ,"CLUB REEF"
            ,"CONCORDE"
            ,"CONTINENTAL PLAZA"
            ,"CORAL BEACH"
            ,"CORAL HILLS"
            ,"CORAL SEA"
            ,"CYRENE ISLAND"
            ,"DIVE INN"
            ,"DOMINA CORAL BAY"
            ,"FALCON HILLS"
            ,"FARAANA HEIGHTS"
            ,"FARAANA REEF"
            ,"GHAZALA BEACH"
            ,"GRAND HALOMY"
            ,"GRAND HOTEL"
            ,"GRAND OASIS"
            ,"GRAND ROTANA"
            ,"H.SHARKS BAY"
            ,"H.W.FALLS"
            ,"IBEROTEL PALACE"
            ,"JAZ BELVEDERE"
            ,"JAZ MIRABEL"
            ,"JOLIE VILLE GOLF"
            ,"KIROSEIZ"
            ,"LEROYAL HOLIDAY"
            ,"MARRIOTT BEACH"
            ,"MAZAR"
            ,"MONTE CARLO"
            ,"MOVENPICK"
            ,"NOVOTEL"
            ,"NOVOTEL PALMS"
            ,"NUBIAN ISLAND"
            ,"NUBIAN VILLAGE"
            ,"OLD VIC"
            ,"ORIENTAL"
            ,"PALMYRA"
            ,"PANORAMA NAAMA"
            ,"PYRAMISA"
            ,"QUEEN SHARM"
            ,"REEF OASIS"
            ,"REHANA"
            ,"RIXOS"
            ,"ROYAL HOLIDAY"
            ,"ROYAL PARADISE"
            ,"SAVOY"
            ,"SEA BEACH"
            ,"SEA BREEZE"
            ,"SEA CLUB"
            ,"SENSATORI"
            ,"SHARM CLIFF"
            ,"SHARM HOLIDAY"
            ,"SHARM INN AMARIEN"
            ,"SHARM PLAZA"
            ,"SHARM REEF"
            ,"SHARMING INN"
            ,"SHORES GOLDEN"
            ,"SIERRA"
            ,"SIVA SHARM"
            ,"SOLYMAR NAAMA"
            ,"ST.GEORGE"
            ,"STELLA DIMARE"
            ,"SULTAN GARDEN"
            ,"TAMRA BEACH"
            ,"TROPITEL NAAMA"
            ,"TURQUOISE BEACH"
            ,"VERA CLUB"
            ,"VERGINIA"
        ];

        foreach ($hotels as $hotel) {
            Hotel::firstOrCreate(['name' => $hotel]);
        }
    }
}
