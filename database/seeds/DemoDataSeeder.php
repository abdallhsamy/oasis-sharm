<?php

use App\Models\Bank;
use App\Models\BankAccount;
use App\Models\Company;
use App\Models\Driver;
use App\Models\Excursion;
use App\Models\ExcursionDetail;
use App\Models\Hotel;
use App\Models\Payee;
use App\Models\Room;
use App\Models\Transport;
use App\Models\Trip;
use App\Models\Vehicle;
use App\Models\VehicleConfig;
use App\Models\VehicleTripType;
use App\Models\VehicleType;
use App\Models\Voucher;
use App\TransportDetail;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DemoDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //          banks and bank accounts //
        $bank = Faker::create(Bank::class);

        for ($i = 1; $i <= 5; $i++) {
            $bankId = Bank::firstOrCreate(['name' => $bank->name])->id;
            for ($x = 1; $x < 10; $x++) {
                BankAccount::firstOrCreate([ 'number' => $x, 'bank_id' => $bankId]);
            }
        } //     end of banks and bank accounts //

        //          Hotel Rooms //
        foreach (Hotel::all() as $hotel) {
            for ($v = 1; $v <= 10; $v++) {
                Room::firstOrCreate(['number' => $v, 'hotel_id' => $hotel->id]);
            }
        } //     end of Hotel Rooms //

        //          Compmany   //
        $companyFaker = Faker::create(Company::class);
        for ($i = 1; $i <= 5; $i++) {
            Company::firstOrCreate(['name' => $companyFaker->name]);
        } //          End  of company //

        //          Driver   //
        $driverFaker = Faker::create(Driver::class);
        for ($i = 1; $i <= 5; $i++) {
            Driver::firstOrCreate(['name' => $driverFaker->name, 'mobile' => $driverFaker->e164PhoneNumber]);
        } //          End  of Driver //

        //          Voucher   //
        foreach (Company::all() as $company) {
            Voucher::firstOrCreate(['company_id' => $company->id,'code' => $bank->countryCode,'number' => $bank->buildingNumber]);
        }  //          end ofVoucher   //

         //          Excursoins   //
        $excursionFaker = Faker::create(Excursion::class);
        for ($i = 1; $i <= 10; $i++) {
            $excursion = Excursion::firstOrCreate([
                'date' => $excursionFaker->date($format = 'Y-m-d', $max = 'now'),
                'voucher_id' => $excursionFaker->biasedNumberBetween($min = 1, $max = Voucher::all()->count(), $function = 'sqrt'),
                'room_id' => $excursionFaker->biasedNumberBetween($min = 1, $max = Room::all()->count(), $function = 'sqrt'),
                'trip_id' => $excursionFaker->biasedNumberBetween($min = 1, $max = Trip::all()->count(), $function = 'sqrt'),
                'plus' => $excursionFaker->biasedNumberBetween($min = 100, $max = 250, $function = 'sqrt'),
                'notes' => $excursionFaker->sentence($nbWords = 6, $variableNbWords = true),
            ]);

            ExcursionDetail::firstOrCreate([
                'excursion_id' => $excursion->id,
                'scooter_id' =>  1,
                'scooter_type_id' => $excursionFaker->biasedNumberBetween($min = 1, $max = 2, $function = 'sqrt'),
                'count' => $excursionFaker->biasedNumberBetween($min = 1, $max = 4, $function = 'sqrt'),
                'price' => $excursionFaker->biasedNumberBetween($min = 100, $max = 250, $function = 'sqrt'),
            ]);

            ExcursionDetail::firstOrCreate([
                'excursion_id' => $excursion->id,
                'scooter_id' =>  2,
                'scooter_type_id' => $excursionFaker->biasedNumberBetween($min = 1, $max = 3, $function = 'sqrt'),
                'count' => $excursionFaker->biasedNumberBetween($min = 1, $max = 4, $function = 'sqrt'),
                'price' => $excursionFaker->biasedNumberBetween($min = 100, $max = 250, $function = 'sqrt'),
            ]);
        }         //          End ofExcursoins   //

        // Payees  //
        $payeeFaker = Faker::create(Payee::class);
        for ($p = 0; $p < 25; $p++) { 
            Payee::firstOrCreate(['name' => $payeeFaker->name]);
        }  // end of Payees  //

        // Vehicles  //
        $vehicleFaker = Faker::create(Vehicle::class);
        for ($p = 1; $p <= 10; $p++) {
            Vehicle::firstOrCreate([
                'number' => $vehicleFaker->tld .  $vehicleFaker->numerify('###'),
                'type_id' => $vehicleFaker->biasedNumberBetween($min = 1, $max = VehicleType::all()->count(), $function = 'sqrt'),
                'driver_id' => $p,
                'licence_id' => $vehicleFaker->biasedNumberBetween($min = 1, $max = 66666, $function = 'sqrt'),
            ]);
        } // end of Vehicles  //
        
         // Transport  //
        $transportFaker = Faker::create(Transport::class);
        for ($p = 1; $p <= 10; $p++) { 
            $transport = Transport::firstOrCreate([
                'date' => $transportFaker->date($format = 'Y-m-d', $max = 'now'),
                'vehicle_id' => $transportFaker->biasedNumberBetween($min = 1, $max = Vehicle::all()->count(), $function = 'sqrt'),
                'vehicle_config_id' => $p,
                'driver_id' => $transportFaker->biasedNumberBetween($min = 1, $max = Driver::all()->count(), $function = 'sqrt'),
                'notice' => $transportFaker->sentence($nbWords = 6, $variableNbWords = true),
                'commission' => 150,
                'plus' => $transportFaker->biasedNumberBetween($min = 1, $max = 250, $function = 'sqrt'),
                'notes' => $transportFaker->sentence($nbWords = 6, $variableNbWords = true),
            ]);

            for ($l = 1; $l <= 5; $l++) { 
                $transportDetails = TransportDetail::firstOrCreate([
                    'transport_id' =>  $transport->id,
                    'vehicle_trip_type_id' =>  $transportFaker->biasedNumberBetween($min = 1, $max = VehicleTripType::all()->count(), $function = 'sqrt'),
                    'count' =>  $transportFaker->biasedNumberBetween($min = 1, $max = 5, $function = 'sqrt'),
                    'price' =>   $transportFaker->biasedNumberBetween($min = 150, $max = 250, $function = 'sqrt')
                ]);
            }


        }  // end of Transport  //
    }
}
