<?php

use App\Models\Scooter;
use App\Models\ScooterPrice;
use App\Models\ScooterType;
//use Faker\Generator as Faker;
use App\Models\Trip;
use Illuminate\Database\Seeder;


class ScooterPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();
//        $faker = Faker::create();
        $scooters = [
            'Quad'  =>  [
                'Single',
                'Double'
            ],
            'Buggy'  =>  [
                'Single',
                'Double',
                '4x4'
            ]
        ];
        $trips = [
            'NORMAL',
            'SUPER VIP SAFARI',
            'ECONOMY',
            'SAFARI SUPER',
            'SAFARI VIP DINNER',
            'MEGA',
            'SAFARI NORMAL',
            'SUPER',
        ];

        foreach ($trips as $trip) {
            $trip = Trip::firstOrCreate(['name' => $trip]);
            foreach ($scooters as $scooter=>$types) {
                $scooter = Scooter::firstOrCreate(['name' => $scooter]);
                foreach ($types as $type) {
                    $scooterType = ScooterType::firstOrCreate(['name' => $type]);
                    ScooterPrice::firstOrCreate([
                        'scooter_id' =>  $scooter->id,
                        'scooter_type_id' => $scooterType->id,
                        'trip_id' => $trip->id
                    ],['price' => $faker->numerify('###')]);
                }
            }
        }

    }
}
