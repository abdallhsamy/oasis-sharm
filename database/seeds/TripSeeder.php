<?php

use App\Models\Trip;
use Illuminate\Database\Seeder;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trips = [
            'NORMAL',
            'SUPER VIP SAFARI',
            'ECONOMY',
            'SAFARI SUPER',
            'SAFARI VIP DINNER',
            'MEGA',
            'SAFARI NORMAL',
            'SUPER',
        ];

        foreach ($trips as $trip) {
            Trip::firstOrCreate(['name' => $trip]);
        }
    }
}
