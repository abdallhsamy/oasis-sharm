<?php

use App\Models\VehicleType;
use Illuminate\Database\Seeder;

class VehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Hi ace', 'CR', 'BUS ABBAS', 'Mosa', 'Bazar', 'waled', 'BUS SUN'];

        foreach ($types as $name) {
            VehicleType::firstOrCreate(['name' => $name]);
        }
    }
}
