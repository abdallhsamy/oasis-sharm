<?php

use App\Models\VehicleConfig;
use App\Models\VehicleTripType;
use App\Models\VehicleType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vehicle_trip_types = ['ذهاب و عودة', 'فجر', 'دهب', 'جبل', 'جبل ½', 'تجميع دهب'];
        $prices  = [
            'Hi ace'    => [150, 150, 1250, 225, 175, 150],
            'CR'        => [325, 300, 2250, 425, 300, 325],
            'BUS ABBAS' => [3850, 0, 0, 0, 0, 0],
            'Mosa'      => [175, 175, 1250, 225, 175, 175],
            'Bazar'     => [175, 0, 0, 0, 0, 0],
            'waled'     => [150, 150, 1250, 225, 175, 150],
            'BUS SUN'   => [3700, 0, 0, 0, 0, 0],
        ];
        foreach ($prices as $vehicle_type_name => $price_list) {
            $vehicle_type = VehicleType::firstOrCreate(['name' => $vehicle_type_name]);
            foreach ($vehicle_trip_types as $key =>  $vehicle_trip_type) {
                $type = VehicleTripType::firstOrCreate(['name' => $vehicle_trip_type]);
                if ($price_list[$key] > 0) {
                    VehicleConfig::firstOrCreate([
                        'vehicle_type_id' => $vehicle_type->id,
                        'vehicle_trip_type_id' => $type->id,
                        'price' => $price_list[$key]
                    ]);
                }
            }
        }
    }
}
