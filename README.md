
## OASIS SHARM


### How To Install
* ``git clone git@bitbucket.org:abdallhsamy/oasis-sharm.git``
* ``cp .env.example .env``
* configure your database credentials in ``.env`` file in project root
* ``composer install``
* ``php artisan migrate --seed``
* ``npm install``
* ``npm run dev``
