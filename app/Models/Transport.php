<?php

namespace App\Models;

use App\TransportDetail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transport extends Model
{
    // use SoftDeletes;

    protected $table = 'transports';

    protected $fillable = ['date', 'vehicle_id', 'driver_id', 'notice', 'plus', 'notes','commission'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['driver', 'created_at', 'updated_at'];

    protected $with = ['vehicle', 'vehicleConfigs'];

    protected $appends = ['driver_name', 'vehicle_number', 'vehicle_type_name', 'value'];

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class, 'vehicle_id');
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driver_id');
    }

//    public function transportAmounts()      //    هنغير اسمها لــ transport_detaiils
//    {
//        return $this->hasMany(TransportAmount::class, 'transport_id');
//    }

    public function vehicleConfigs()         //  اسعار الرحلات
    {
        return $this->belongsToMany(VehicleConfig::class);
    }

    public function details()
    {
        return $this->hasMany(TransportDetail::class, 'transport_id');
    }

    public function calculatCommission()
    {
        $commission = $E = $O = $G = $I = $K = $M = 0 ;
        $letters = ['E' => 'ذهاب و عودة', 'O' => 'تجميع دهب', 'G' => 'فجر', 'I' => 'دهب', 'K' => 'جبل', 'M' => 'جبل ½'];

        foreach ($this->details as $detail) {
            foreach ($letters as $letter=> $type) {
                if ($detail->vehicle_trip_type_id === VehicleTripType::where('name', $type)->first()->id) ${$letter}  = $detail->count;
            }
        }
        if (($E+$O)>10)  $commission += ((($E+$O)-10)*15);
        $commission += (($G*15)+($I*50)+($K*15)+($M*15));

        return $commission;
    }

    public function getDriverNameAttribute()
    {
        return $this->driver->name;
    }

    public function getVehicleNumberAttribute()
    {
        return $this->vehicle->number;
    }

    public function getVehicleTypeNameAttribute()
    {
        return $this->vehicle->type_name;
    }

    public function getValueAttribute()
    {
        return $this->commission + $this->plus;
    }
}
