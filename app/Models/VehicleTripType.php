<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleTripType extends Model
{
    protected $fillable = ['name'];

    protected $table = 'vehicle_trip_types';

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * The roles that belong to the user.
     */
    public function vehicleTypes()
    {
        return $this->belongsToMany(VehicleType::class, 'vehicle_configs', 'vehicle_trip_type_id', 'vehicle_type_id')->withPivot('price');
    }


}
