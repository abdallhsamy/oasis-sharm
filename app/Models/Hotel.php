<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    // use SoftDeletes;

    protected $table = 'hotels';

    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['rooms', 'created_at', 'updated_at'];

    protected $appends = ['rooms_count'];

    public function rooms()
    {
        return $this->hasMany(Room::class, 'hotel_id');
    }

    public function getRoomsCountAttribute()
    {
        return $this->rooms->count();
    }
}
