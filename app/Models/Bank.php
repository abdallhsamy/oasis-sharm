<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
    // use SoftDeletes;

    protected $table = 'banks';

    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['accounts', 'created_at', 'updated_at'];

    protected $appends = ['accounts_count'];

    public function accounts()
    {
        return $this->hasMany(BankAccount::class, 'bank_id');
    }

    public function getAccountsCountAttribute()
    {
        return $this->accounts->count();
    }
}
