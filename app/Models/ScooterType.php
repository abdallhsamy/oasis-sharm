<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScooterType extends Model
{
    // use SoftDeletes;

    protected $table = 'scooter_types';

    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at'];

    public function scooters()
    {
        return $this->belongsToMany(Scooter::class,  'scooter_type','scooter_type_id');
    }


    public function excursionDetails()
    {
        return $this->hasMany(ExcursionDetail::class,'scooter_type_id');
    }
}
