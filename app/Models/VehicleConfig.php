<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VehicleConfig extends Model
{
    protected $fillable = ['vehicle_type_id', 'vehicle_trip_type_id' ,'price'];



    protected $appends = ['vehicle_type_name', 'vehicle_trip_type_name'];

    public function transports()
    {
        return $this->belongsToMany(Transport::class);
    }

    public function getVehicleTypeNameAttribute()
    {
        return VehicleType::find($this->vehicle_type_id)->name;
    }
    public function getVehicleTripTypeNameAttribute()
    {
        return VehicleTripType::find($this->vehicle_trip_type_id)->name;
    }



}
