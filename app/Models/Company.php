<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    // use SoftDeletes;

    protected $table = 'companies';

    protected $fillable = ['name'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = ['vouchers', 'created_at', 'updated_at'];

    protected $appends = ['vouchers_count'];


    public function vouchers()
    {
        return $this->hasMany(Voucher::class, 'company_id');
    }

    public function getVouchersCountAttribute()
    {
        return $this->vouchers->count();
    }
}
