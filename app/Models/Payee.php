<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payee extends Model
{
    // use SoftDeletes;

    protected $table = 'payees';

    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['transactions', 'created_at', 'updated_at'];

    protected $appends = ['transactions_count'];

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'payee_id');
    }

    public function getTransactionsCountAttribute()
    {
        return $this->transactions->count();
    }
}
