<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Licence extends Model
{
    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'licence_id');
    }
}
