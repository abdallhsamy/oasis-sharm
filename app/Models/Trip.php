<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trip extends Model
{
    // use SoftDeletes;

    protected $table = 'trips';

    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['excursions', 'created_at', 'updated_at'];

    protected $appends = ['excursions_count'];

    public function excursions()
    {
        return $this->hasMany(Excursion::class, 'trip_id');
    }

    public function getExcursionsCountAttribute()
    {
        return $this->excursions->count();
    }
}
