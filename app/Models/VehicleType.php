<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleType extends Model
{
    // use SoftDeletes;

    protected $table = 'vehicle_types';

    protected $fillable = ['id', 'name'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['vehicles', 'created_at', 'updated_at'];

    protected $appends = ['vehicles_count'];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'type_id');
    }

    /**
     * The roles that belong to the user.
     */
    public function vehicleTripTypes()
    {
        return $this->belongsToMany(VehicleTripType::class, 'vehicle_configs', 'vehicle_type_id', 'vehicle_trip_type_id')->withPivot('price');
    }

    public function getVehiclesCountAttribute()
    {
        return $this->vehicles->count();
    }
}
