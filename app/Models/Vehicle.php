<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    // use SoftDeletes;

    protected $table = 'vehicles';

    protected $fillable = ['number', 'type_id', 'driver_id', 'licence_id'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['type', 'driver', 'created_at', 'updated_at'];

    protected $appends = ['type_name', 'driver_name'];


    public function vehicleType()
    {
        return $this->belongsTo(VehicleType::class, 'type_id');
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class, 'driver_id');
    }

//    public function licence()
//    {
//        return $this->belongsTo(Licence::class, 'licence_id');
//    }

    public function transports()
    {
        return $this->hasMany(Transport::class, 'vehicle_id');
    }

    public function getTypeNameAttribute()
    {
        return $this->vehicleType ? $this->vehicleType->name : '';
    }

    public function getDriverNameAttribute()
    {
        return $this->driver ? $this->driver->name : '';
    }

//    public function getLicenceNumberAttribute()
//    {
//        return $this->licence ? $this->licence->number : '';
//    }

}
