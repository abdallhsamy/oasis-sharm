<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransportAmountPrice extends Model
{
    // use SoftDeletes;

    protected $table = 'transport_amount_prices';

    protected $fillable = [
        'vehicle_type_id',
        'price'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transportAmounts()
    {
        return $this->hasMany(TransportAmount::class, 'transport_amount_price_id');
    }
}
