<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportAmount extends Model
{
    protected $table = 'transport_amounts';

    protected $fillable = [
        'transport_amount_price_id',
        'transport_id',
        'number'
    ];

    public function transport()
    {
        return $this->belongsTo(Transport::class, 'transport_id');
    }

    public function transportAmountPrice()
    {
        return $this->belongsTo(TransportAmountPrice::class, 'transport_amount_price_id');
    }
}
