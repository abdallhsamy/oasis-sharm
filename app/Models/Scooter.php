<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scooter extends Model
{
    // use SoftDeletes;

    protected $table = 'scooters';

    protected $fillable = ['name'];

    protected $dates = ['created_at', 'updated_at'];


    public function scooterTypes()
    {
        return $this->belongsToMany(ScooterType::class,  'scooter_type','scooter_type_id');
    }


    public function excursionDetails()
    {
        return $this->hasMany(ExcursionDetail::class,'scooter_id');
    }

    // public function excursion()
    // {
    //     return $this->belongsTo(Excursion::class, 'excursion_id');
    // }

    // public function types()
    // {
    //     return $this->hasMany(ExcursionVehicleType::class, 'excursion_vehicle_id');
    // }
}
