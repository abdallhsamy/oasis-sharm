<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScooterPrice extends Model
{
    protected $fillable = ['scooter_id', 'scooter_type_id',	'trip_id', 'price'];

    public function scooter()
    {
        return $this->belongsTo(Scooter::class, 'scooter_id');
    }

    public function scooterType()
    {
        return $this->belongsTo(ScooterType::class, 'scooter_type_id');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id');
    }
}
