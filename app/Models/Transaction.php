<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    // use SoftDeletes;

    protected $table = 'transactions';

    protected $fillable = [ 'bank_account_id', 'date', 'transaction_type', 'check_number', 'payee_id', 'is_due', 'notes'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['bankAccount', 'payee', 'created_at', 'updated_at'];

    protected $appends = ['bank_account_name', 'payee_name'];


    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'bank_account_id');
    }

    public function payee()
    {
        return $this->belongsTo(Payee::class, 'payee_id');
    }

    public function getBankAccountNameAttribute()
    {
        return $this->bankAccount->name;
    }

    public function getPayeeNameAttribute()
    {
        return $this->payee->name;
    }
}
