<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    // use SoftDeletes;

    protected $table = 'drivers';

    protected $fillable = [
        'name',
        'mobile'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function transports()
    {
        return $this->hasMany(Transport::class, 'driver_id');
    }
}
