<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    // use SoftDeletes;

    protected $table = 'rooms';

    protected $fillable = ['hotel_id', 'number'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['hotel', 'created_at', 'updated_at'];

    protected $appends = ['hotel_name'];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'hotel_id');
    }

    public function excursions()
    {
        return $this->hasMany(Excursion::class, 'room_id');
    }

    public function getHotelNameAttribute()
    {
        return $this->hotel->name;
    }
}
