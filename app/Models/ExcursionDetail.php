<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExcursionDetail extends Model
{
    protected $fillable = [ 'excursion_id',  'scooter_id',  'scooter_type_id',  'count',  'price', 'total_price'];


    public function excursion()
    {
        return $this->belongsTo(Excursion::class, 'excursion_id');
    }

    public function scooter()
    {
        return $this->belongsTo(Scooter::class, 'scooter_id');
    }

    public function scooterType()
    {
        return $this->belongsTo(ScooterType::class, 'scooter_type_id');
    }

    public function getTotalPriceAttribute()
    {
        return $this->price * $this->count;
    }
}
