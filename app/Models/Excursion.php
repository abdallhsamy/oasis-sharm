<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Excursion extends Model
{
    // use SoftDeletes;

    protected $table = 'excursions';

    protected $fillable = ['date', 'voucher_id', 'room_id', 'trip_id', 'plus', 'notes'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['created_at', 'updated_at' ,'room' , 'trip', 'voucher', 'excursion_details'];

    protected $appends = ['voucher_code', 'voucher_number', 'hotel_name', 'room_number', 'trip_name', 'price','total', 'scooters'];

    public function getScootersAttribute()
    {
        $scooters = [];
        foreach (DB::table('excursion_scooter')->where('excursion_id', $this->id)->get() as $pivot) {
            $scooterData = [];
            $scooterData['name'] = Scooter::findOrFail($pivot->scooter_id)->name;
            $scooterData['type'] = ScooterType::findOrFail($pivot->type_id)->name;
            $scooters[] = $scooterData;
        }
        return $scooters;
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id');
    }

    public function voucher()
    {
        return $this->belongsTo(Voucher::class, 'voucher_id');
    }

    public function excursionDetails()
    {
        return $this->hasMany(ExcursionDetail::class,'excursion_id');
    }

    public function getVoucherCodeAttribute()
    {
        return $this->voucher->code;
    }

    public function getVoucherNumberAttribute()
    {
        return $this->voucher->number;
    }

    public function getHotelNameAttribute()
    {
        return $this->room->hotel->name;
    }

    public function getRoomNumberAttribute()
    {
        return $this->room->number;
    }

    public function getTripNameAttribute()
    {
        return $this->trip->name;
    }
    public function getDetailsAttribute()
    {
        return ExcursionDetail::where('excursion_id', $this->id)->get();
    }

    public function calculatePrice()
    {
        $total = 0;
        foreach ($this->excursionDetails as $detail) {
            $total += ($detail->price * $detail->count);
        }
        return $total;
    }


    public function getPriceAttribute()
    {
        return $this->calculatePrice();
    }

    public function getTotalAttribute()
    {
        return $this->calculatePrice() + $this->plus;
    }
}
