<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    // use SoftDeletes;

    protected $table = 'bank_accounts';

    protected $fillable = ['name', 'number', 'bank_id'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['bank', 'created_at', 'updated_at'];

    protected $appends = ['bank_name'];

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'bank_account_id');
    }

    public function getBankNameAttribute()
    {
        return $this->bank->name;
    }
}
