<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    // use SoftDeletes;

    protected $table = 'vouchers';

    protected $fillable = ['code', 'number', 'company_id'];

    protected $dates = ['created_at', 'updated_at'];

    protected $hidden = ['company', 'created_at', 'updated_at'];

    protected $appends = ['company_name'];

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function excursions()
    {
        return $this->hasMany(Excursion::class, 'voucher_id');
    }

    public function getCompanyNameAttribute()
    {
        return $this->company->name;

    }
}
