<?php


function res($success = true, $message = '', $data = null, $errors = null)
{
    $data = $data == null ? new stdClass : $data;
    if (!is_null($errors)) {
        return response()->json([
            'success' => $success,
            'message' => $message,
            'errors' => $errors
        ]);
    }

    return response()->json([
        'success' => $success,
        'message' => $message,
        'data' => $data
    ]);
}

