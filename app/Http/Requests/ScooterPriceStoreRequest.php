<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScooterPriceStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'scooter_id' => ['required', 'integer'],
            'scooter_type_id' => ['required', 'integer'],
            'trip_id' => ['required', 'integer'],
            'price' => ['required', 'numeric'],
        ];
    }
}
