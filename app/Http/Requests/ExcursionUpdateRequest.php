<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExcursionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required', 'date'],
            'voucher_id' => ['required', 'integer'],
            'room_id' => ['required', 'integer'],
            'trip_id' => ['required', 'integer'],
//            'plus' => ['required', 'decimal'],
            'notes' => []
        ];
    }
}
