<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransportUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required'],
            'vehicle_id' => ['required', 'integer'],
            'vehicle_config_id' => ['required', 'integer'],
            'driver_id' => ['integer'],
            'plus' => [],
            'notice' => [],
            'notes' => []
        ];
    }
}
