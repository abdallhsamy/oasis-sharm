<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransportAmountUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transport_amount_price_id' => ['required', 'integer'],
            'transport_id' => ['required', 'integer'],
            'number' => ['require', 'integerd']
        ];
    }
}
