<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TransactionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_account_id' => ['required', 'integer'],
            'date' => ['required', 'date'],
            'transaction_type' => ['required', 'in:debit,credit'],
            'check_number' => ['required', 'integer'],
            'payee_id' => ['required', 'integer'],
            // 'is_due' => ['required', 'in:0,1'],
            'notes' => []
        ];
    }
}
