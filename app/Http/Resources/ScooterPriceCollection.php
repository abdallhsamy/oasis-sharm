<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ScooterPriceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {
        return $this->collection->transform(function ($item) {
            return [
                'id' => $item->id,
                'scooter_id' => $item->scooter_id,
                'scooter_type_id' => $item->scooter_type_id,
                'trip_id' => $item->trip_id,
                'price' => $item->price,
                'scooter_name' => $item->scooter->name,
                'scooter_type_name' => $item->scooterType->name,
                'trip_name' => $item->trip->name,
            ];
        });
    }

}
