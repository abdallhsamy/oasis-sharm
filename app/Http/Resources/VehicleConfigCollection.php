<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class VehicleConfigCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        return parent::toArray($request);
        return [
            'data' => $this->collection->transform(function($config){
                return [
                    'id' => $config->id,
                    "vehicle_type_id"               => $config->vehicle_type_id,
                    "vehicle_trip_type_id"          => $config->vehicle_trip_type_id,
                    "price"                         => $config->price,
                    "vehicle_type_name"             => $config->vehicle_type_name,
                    "vehicle_trip_type_name"        => $config->vehicle_trip_type_name,
                ];
            }),
        ];
    }
}
