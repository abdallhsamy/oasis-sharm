<?php

namespace App\Http\Controllers;

use App\ScooterType;
use Illuminate\Http\Request;

class ScooterTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ScooterType  $scooterType
     * @return \Illuminate\Http\Response
     */
    public function show(ScooterType $scooterType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ScooterType  $scooterType
     * @return \Illuminate\Http\Response
     */
    public function edit(ScooterType $scooterType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ScooterType  $scooterType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ScooterType $scooterType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ScooterType  $scooterType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScooterType $scooterType)
    {
        //
    }
}
