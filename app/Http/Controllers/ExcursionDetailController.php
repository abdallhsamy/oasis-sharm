<?php

namespace App\Http\Controllers;

use App\Models\ExcursionDetail;
use Illuminate\Http\Request;

class ExcursionDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExcursionDetail  $excursionDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ExcursionDetail $excursionDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExcursionDetail  $excursionDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(ExcursionDetail $excursionDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExcursionDetail  $excursionDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExcursionDetail $excursionDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExcursionDetail  $excursionDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExcursionDetail $excursionDetail)
    {
        //
    }
}
