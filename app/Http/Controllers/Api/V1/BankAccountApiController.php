<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\BankAccountStoreRequest;
use App\Http\Requests\BankAccountUpdateRequest;
use App\Http\Resources\BankAccountResource;
use App\Http\Resources\BankResource;
use App\Models\Bank;
use App\Models\BankAccount;
use Symfony\Component\HttpFoundation\Response;

class BankAccountApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new BankAccountResource(BankAccount::all());
    }

    /**
     * @return BankResource
     */
    public function create()
    {
        return new BankResource(Bank::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param BankAccountStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BankAccountStoreRequest $request)
    {
        $bank_account = BankAccount::create($request->all());
        return (new BankAccountResource($bank_account))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param BankAccount $bank_account
     * @return BankAccountResource
     */
    public function show(BankAccount $bank_account)
    {
        return new BankAccountResource($bank_account);
    }

    /**
     * Update the specified resource in storage.
     * @param BankAccountUpdateRequest $request
     * @param BankAccount $bank_account
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BankAccountUpdateRequest $request, BankAccount $bank_account)
    {

        $bank_account->update($request->all());

        return (new BankAccountResource($bank_account))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param BankAccount $bank_account
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(BankAccount $bank_account)
    {
        $bank_account->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
