<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\TransportAmountPriceStoreRequest;
use App\Http\Requests\TransportAmountPriceUpdateRequest;
use App\Http\Resources\TransportAmountPriceResource;
use App\Models\TransportAmountPrice;
use Symfony\Component\HttpFoundation\Response;

class TransportAmountPriceApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new TransportAmountPriceResource(TransportAmountPrice::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param TransportAmountPriceStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TransportAmountPriceStoreRequest $request)
    {
        $transport_amount_price = TransportAmountPrice::create($request->all());
        return (new TransportAmountPriceResource($transport_amount_price))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param TransportAmountPrice $transport_amount_price
     * @return TransportAmountPriceResource
     */
    public function show(TransportAmountPrice $transport_amount_price)
    {
        return new TransportAmountPriceResource($transport_amount_price);
    }

    /**
     * Update the specified resource in storage.
     * @param TransportAmountPriceUpdateRequest $request
     * @param TransportAmountPrice $transport_amount_price
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TransportAmountPriceUpdateRequest $request, TransportAmountPrice $transport_amount_price)
    {
        $transport_amount_price->update($request->all());

        return (new TransportAmountPriceResource($transport_amount_price))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param TransportAmountPrice $transport_amount_price
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(TransportAmountPrice $transport_amount_price)
    {
        $transport_amount_price->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
