<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\HotelStoreRequest;
use App\Http\Requests\HotelUpdateRequest;
use App\Http\Resources\HotelResource;
use App\Models\Hotel;
use Symfony\Component\HttpFoundation\Response;

class HotelApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new HotelResource(Hotel::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param HotelStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(HotelStoreRequest $request)
    {
        $hotel = Hotel::create($request->all());
        return (new HotelResource($hotel))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Hotel $hotel
     * @return HotelResource
     */
    public function show(Hotel $hotel)
    {
        return new HotelResource($hotel);
    }

    /**
     * Update the specified resource in storage.
     * @param HotelUpdateRequest $request
     * @param Hotel $hotel
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(HotelUpdateRequest $request, Hotel $hotel)
    {
        $hotel->update($request->all());

        return (new HotelResource($hotel))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Hotel $hotel
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
