<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\BankStoreRequest;
use App\Http\Requests\BankUpdateRequest;
use App\Http\Resources\BankResource;
use App\Models\Bank;
use Illuminate\Http\Response;

class BankApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new BankResource(Bank::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param BankStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BankStoreRequest $request)
    {

        $bank = Bank::create($request->all());
        return (new BankResource($bank))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Bank $bank
     * @return BankResource
     */
    public function show(Bank $bank)
    {
        return new BankResource($bank);
    }

    /**
     * Update the specified resource in storage.
     * @param BankUpdateRequest $request
     * @param Bank $bank
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BankUpdateRequest $request, Bank $bank)
    {
        $bank->update($request->all());

        return (new BankResource($bank))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Bank $bank
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Bank $bank)
    {
        $bank->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
