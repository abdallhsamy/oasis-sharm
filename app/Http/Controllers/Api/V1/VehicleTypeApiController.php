<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\VehicleTypeStoreRequest;
use App\Http\Requests\VehicleTypeUpdateRequest;
use App\Http\Resources\VehicleTypeResource;
use App\Models\VehicleType;
use Symfony\Component\HttpFoundation\Response;

class VehicleTypeApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new VehicleTypeResource(VehicleType::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param VehicleTypeStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleTypeStoreRequest $request)
    {
        $vehicle_type = VehicleType::create($request->all());
        return (new VehicleTypeResource($vehicle_type))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param VehicleType $vehicle_type
     * @return VehicleTypeResource
     */
    public function show(VehicleType $vehicle_type)
    {
        return new VehicleTypeResource($vehicle_type);
    }

    /**
     * Update the specified resource in storage.
     * @param VehicleTypeUpdateRequest $request
     * @param VehicleType $vehicle_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VehicleTypeUpdateRequest $request, VehicleType $vehicle_type)
    {
        $vehicle_type->update($request->all());

        return (new VehicleTypeResource($vehicle_type))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param VehicleType $vehicle_type
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(VehicleType $vehicle_type)
    {
        $vehicle_type->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
