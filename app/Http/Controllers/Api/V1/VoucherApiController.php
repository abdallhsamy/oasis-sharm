<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\VoucherStoreRequest;
use App\Http\Requests\VoucherUpdateRequest;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\VoucherResource;
use App\Models\Company;
use App\Models\Voucher;
use Symfony\Component\HttpFoundation\Response;

class VoucherApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new VoucherResource(Voucher::all());
    }

    /**
     * @return CompanyResource
     */
    public function create()
    {
        return new CompanyResource(Company::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param VoucherStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VoucherStoreRequest $request)
    {
        $voucher = Voucher::create($request->all());
        return (new VoucherResource($voucher))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Voucher $voucher
     * @return VoucherResource
     */
    public function show(Voucher $voucher)
    {
        return new VoucherResource($voucher);
    }

    /**
     * Update the specified resource in storage.
     * @param VoucherUpdateRequest $request
     * @param Voucher $voucher
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VoucherUpdateRequest $request, Voucher $voucher)
    {
        $voucher->update($request->all());

        return (new VoucherResource($voucher))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Voucher $voucher
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Voucher $voucher)
    {
        $voucher->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
