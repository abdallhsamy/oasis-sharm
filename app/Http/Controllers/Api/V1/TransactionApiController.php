<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\TransactionStoreRequest;
use App\Http\Requests\TransactionUpdateRequest;
use App\Http\Resources\TransactionResource;
use App\Models\BankAccount;
use App\Models\Payee;
use App\Models\Transaction;
use Symfony\Component\HttpFoundation\Response;

class TransactionApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new TransactionResource(Transaction::all());
    }

    /**
     * @return TransactionResource
     */
    public function create()
    {
        return new TransactionResource([
            'bank_accounts' => BankAccount::all(),
            'transaction_types' => ['debit', 'credit'],
            'payees' => Payee::all(),
        ]);
    }
    /**
     * Store a newly created resource in storage.
     * @param TransactionStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TransactionStoreRequest $request)
    {
        $transaction = Transaction::create($request->all());
        return (new TransactionResource($transaction))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Transaction $transaction
     * @return TransactionResource
     */
    public function show(Transaction $transaction)
    {
        return new TransactionResource($transaction);
    }

    /**
     * Update the specified resource in storage.
     * @param TransactionUpdateRequest $request
     * @param Transaction $transaction
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TransactionUpdateRequest $request, Transaction $transaction)
    {
        $transaction->update($request->all());

        return (new TransactionResource($transaction))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Transaction $transaction
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Transaction $transaction)
    {
        $transaction->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
