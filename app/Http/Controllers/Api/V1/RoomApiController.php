<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\RoomStoreRequest;
use App\Http\Requests\RoomUpdateRequest;
use App\Http\Resources\HotelResource;
use App\Http\Resources\RoomResource;
use App\Models\Hotel;
use App\Models\Room;
use Symfony\Component\HttpFoundation\Response;

class RoomApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new RoomResource(Room::all());
    }

    /**
     * @return HotelResource
     */
    public function create()
    {
        return new HotelResource(Hotel::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param RoomStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoomStoreRequest $request)
    {
        $room = Room::create($request->all());
        return (new RoomResource($room))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Room $room
     * @return RoomResource
     */
    public function show(Room $room)
    {
        return new RoomResource($room);
    }

    /**
     * Update the specified resource in storage.
     * @param RoomUpdateRequest $request
     * @param Room $room
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RoomUpdateRequest $request, Room $room)
    {
        $room->update($request->all());

        return (new RoomResource($room))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Room $room
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Room $room)
    {
        $room->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
