<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\ExcursionVehicleTypeStoreRequest;
use App\Http\Requests\ExcursionVehicleTypeUpdateRequest;
use App\Http\Resources\ExcursionVehicleTypeResource;
use App\Models\ExcursionVehicleType;
use Symfony\Component\HttpFoundation\Response;

class ExcursionVehicleTypeApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new ExcursionVehicleTypeResource(ExcursionVehicleType::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param ExcursionVehicleTypeStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExcursionVehicleTypeStoreRequest $request)
    {
        $excursion_vehicle_type = ExcursionVehicleType::create($request->all());
        return (new ExcursionVehicleTypeResource($excursion_vehicle_type))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param ExcursionVehicleType $excursion_vehicle_type
     * @return ExcursionVehicleTypeResource
     */
    public function show(ExcursionVehicleType $excursion_vehicle_type)
    {
        return new ExcursionVehicleTypeResource($excursion_vehicle_type);
    }

    /**
     * Update the specified resource in storage.
     * @param ExcursionVehicleTypeUpdateRequest $request
     * @param ExcursionVehicleType $excursion_vehicle_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExcursionVehicleTypeUpdateRequest $request, ExcursionVehicleType $excursion_vehicle_type)
    {
        $excursion_vehicle_type->update($request->all());

        return (new ExcursionVehicleTypeResource($excursion_vehicle_type))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param ExcursionVehicleType $excursion_vehicle_type
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(ExcursionVehicleType $excursion_vehicle_type)
    {
        $excursion_vehicle_type->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
