<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\DriverStoreRequest;
use App\Http\Requests\DriverUpdateRequest;
use App\Http\Resources\DriverResource;
use App\Models\Driver;
use Symfony\Component\HttpFoundation\Response;

class DriverApiController extends ApiController
{
    /**
    * Display a listing of the resource.
    */
    public function index()
    {
        return new DriverResource(Driver::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param DriverStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DriverStoreRequest $request)
    {
        $driver = Driver::create($request->all());
        return (new DriverResource($driver))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Driver $driver
     * @return DriverResource
     */
    public function show(Driver $driver)
    {
        return new DriverResource($driver);
    }

    /**
     * Update the specified resource in storage.
     * @param DriverUpdateRequest $request
     * @param Driver $driver
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DriverUpdateRequest $request, Driver $driver)
    {
        $driver->update($request->all());

        return (new DriverResource($driver))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Driver $driver
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Driver $driver)
    {
        $driver->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
