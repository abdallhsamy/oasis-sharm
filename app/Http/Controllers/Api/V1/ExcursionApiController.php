<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\ExcursionStoreRequest;
use App\Http\Requests\ExcursionUpdateRequest;
use App\Http\Resources\ExcursionResource;
use App\Models\Excursion;
use App\Models\ExcursionDetail;
use App\Models\Hotel;
use App\Models\Room;
use App\Models\Trip;
use App\Models\Scooter;
use App\Models\ScooterPrice;
use App\Models\ScooterType;
use App\Models\Voucher;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;

class ExcursionApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new ExcursionResource(Excursion::all());
    }

    public function create()
    {
        return [
            'vouchers' => Voucher::all(),
            'hotels' => Hotel::all(),
            'rooms' => Room::all(),
            'trips' => Trip::all(),
            'scooters' => Scooter::all(),
            'scooter_types' => ScooterType::all()
        ];
    }


    /**
     * Store a newly created resource in storage.
     * @param ExcursionStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExcursionStoreRequest $request)
    {
        $excursion = Excursion::create((is_array($request['details'])) ? Arr::except($request->all(), 'details')  : $request->all());

        if (is_array($request->details)) {
            foreach ($request->details as $detail) {
                $detail['excursion_id'] = $excursion->id;
                $detail['price'] = ScooterPrice::where(['scooter_id' => $detail['scooter_id'], 'scooter_type_id' => $detail['scooter_type_id']])->first()->price;
                ExcursionDetail::create($detail);
            }

        }

        return (new ExcursionResource($excursion))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Excursion $excursion
     * @return ExcursionResource
     */
    public function show(Excursion $excursion)
    {
        return new ExcursionResource($excursion);
    }

    /**
     * Update the specified resource in storage.
     * @param ExcursionUpdateRequest $request
     * @param Excursion $excursion
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExcursionUpdateRequest $request, Excursion $excursion)
    {
        $excursion->update($request->all());

        return (new ExcursionResource($excursion))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Excursion $excursion
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Excursion $excursion)
    {
        $excursion->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
