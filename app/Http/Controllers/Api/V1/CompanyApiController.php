<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Symfony\Component\HttpFoundation\Response;

class CompanyApiController extends ApiController
{
    /**
    * Display a listing of the resource.
    */
    public function index()
    {
        return new CompanyResource(Company::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param CompanyStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CompanyStoreRequest $request)
    {
        $company = Company::create($request->all());
        return (new CompanyResource($company))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Company $company
     * @return CompanyResource
     */
    public function show(Company $company)
    {
        return new CompanyResource($company);
    }

    /**
     * Update the specified resource in storage.
     * @param CompanyUpdateRequest $request
     * @param Company $company
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $company->update($request->all());

        return (new CompanyResource($company))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Company $company
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
