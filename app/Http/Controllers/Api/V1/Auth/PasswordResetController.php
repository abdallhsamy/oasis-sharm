<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Http\Controllers\Controller;
use App\Models\Users\PasswordReset;
use App\Models\Users\User;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PasswordResetController extends Controller
{
    /**
     * Create token password reset
     *
     * @param Request $request
     * @return JsonResponse [string] message
     * @throws Exception
     */
    public function create(Request $request)
    {
        $request->validate(['email' => 'required|string|email']);
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return res(false, 'We can\'t find a user with that e-mail address.')->setStatusCode(404);
        }
        PasswordReset::updateOrCreate(
            ['email' => $user->email], [
            'email' => $user->email,
            'token' => random_int(100000, 999999)
        ]);

        $passwordReset = PasswordReset::where('email', $user->email)->first();
        if ($user && $passwordReset) {
            $user->notify(new PasswordResetRequest($passwordReset->token));

            $message = 'M -' . $passwordReset->token . 'is your reset code';
            sendMessage($user, $message, env('SMS_PROVIDER'));
        }

        return res(true, 'We have e-mailed your password reset link!');
    }

    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return JsonResponse [string] message
     */
    public function find($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset) {
            return res(false, 'This password reset token is invalid.')->setStatusCode(404);
        }
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
            $passwordReset->delete();
            return res(false, 'This password reset token is invalid.')->setStatusCode(404);
        }
        return res(true, 'token accepted', $passwordReset);
    }

    /**
     * Reset password
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function reset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);
        $passwordReset = PasswordReset::where([
            ['token', $request->token],
            ['email', $request->email]
        ])->first();
        if (!$passwordReset)
            return res(false, 'This password reset token is invalid.')->setStatusCode(404);

        $user = User::where('email', $passwordReset->email)->first();
        if (!$user)
            return res(false, 'We can\'t find a user with that e-mail address.')->setStatusCode(404);
        $user->password = bcrypt($request->password);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        return res(true, 'Your password has been reset successfully!', $user);
    }
}
