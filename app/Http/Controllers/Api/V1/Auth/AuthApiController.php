<?php

namespace App\Http\Controllers\Api\V1\Auth;

use URL;
use GuzzleHttp\Client;
use App\Models\Users\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Exception\BadResponseException;


/**
 * Class AuthApiController
 * @package App\Http\Controllers\Api\V1\Auth
 */
class AuthApiController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $http = new Client;
        try {
            $response = $http->post(URL::to('oauth/token'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);
            return json_decode((string)$response->getBody(), true);
        } catch (BadResponseException $e) {
            if ($e->getCode() === 400) {
                return res(false, 'Invalid Request. Please enter a username or a password')->setStatusCode(400);
            }
            if ($e->getCode() === 401) {
                return res(false, 'Your credentials are incorrect. Please try again')->setStatusCode(401);
            }
            return res(false, 'Something went wrong on the server')->setStatusCode($e->getCode());
        }
    }

    /**
     * @return JsonResponse
     */
    public function user(Request $request)
    {
        $user =  $request->user();
        $user = [
            'email' => $user->email,
            'id' => $user->id,
            'name' => $user->name,
            'email_verified_at' => $user->email_verified_at,
            'permissions' => $request->user()->getPermissionsViaRoles()->pluck('name'),
            'role' => $request->user()->getRoleNames(),
        ];

        return res(true, 'logged in successfully', [
            'user' => $user,
            'roles' => Role::all()->pluck('name')
        ]);
    }

    public function logout(Request $request)
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        return res(true, 'logged out successfully');
    }
}
