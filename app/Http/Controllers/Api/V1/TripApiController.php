<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\TripStoreRequest;
use App\Http\Requests\TripUpdateRequest;
use App\Http\Resources\TripResource;
use App\Models\Trip;
use Symfony\Component\HttpFoundation\Response;

class TripApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new TripResource(Trip::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param TripStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TripStoreRequest $request)
    {
        $trip = Trip::create($request->all());
        return (new TripResource($trip))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Trip $trip
     * @return TripResource
     */
    public function show(Trip $trip)
    {
        return new TripResource($trip);
    }

    /**
     * Update the specified resource in storage.
     * @param TripUpdateRequest $request
     * @param Trip $trip
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TripUpdateRequest $request, Trip $trip)
    {
        $trip->update($request->all());

        return (new TripResource($trip))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Trip $trip
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Trip $trip)
    {
        $trip->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
