<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\VehicleConfigStoreRequest;
use App\Http\Requests\VehicleConfigUpdateRequest;
use App\Http\Resources\VehicleConfigCollection;
use App\Http\Resources\VehicleConfigResource;
use App\Models\VehicleConfig;
use App\Models\VehicleTripType;
use App\Models\VehicleType;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VehicleConfigApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return VehicleConfigCollection
     */
    public function index()
    {
        return new VehicleConfigCollection(VehicleConfig::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return VehicleConfigResource
     */
    public function create()
    {
        return new VehicleConfigResource([
            'vehicle_types' => VehicleType::get(['id','name']),
            'vehicle_trip_types' => VehicleTripType::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleConfigStoreRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VehicleConfig  $vehicleConfig
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleConfig $vehicleConfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VehicleConfig  $vehicleConfig
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleConfig $vehicleConfig)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleConfig  $vehicleConfig
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VehicleConfigUpdateRequest $request, VehicleConfig $vehicleConfig)
    {
        $vehicleConfig->update($request->all());

        return (new VehicleConfigResource($vehicleConfig))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VehicleConfig  $vehicleConfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleConfig $vehicleConfig)
    {
        //
    }
}
