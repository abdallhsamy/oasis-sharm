<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Excursion;
use App\Models\Transaction;
use App\Models\Transport;
// use Illuminate\Http\Request;

class ReportApiController extends Controller
{
    private $reportableModels = [
        'transaction' => Transaction::class,
        'excursion' => Excursion::class,
        'transport' => Transport::class,
    ];

    public function  index()
    {
        return view('reports.index');
    }

    /*
     *  $options = [ $filters, $range, $model]
     */

    public function displayReport($options = [])
    {
        
    }
}
