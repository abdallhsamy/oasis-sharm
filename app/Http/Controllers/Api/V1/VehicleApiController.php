<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\VehicleStoreRequest;
use App\Http\Requests\VehicleUpdateRequest;
use App\Http\Resources\VehicleResource;
use App\Http\Resources\VehicleTypeResource;
use App\Models\Driver;
//use App\Models\Licence;
use App\Models\Vehicle;
use App\Models\VehicleType;
use Symfony\Component\HttpFoundation\Response;

class VehicleApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new VehicleResource(Vehicle::all());
    }

    /**
     * @return VehicleTypeResource
     */
    public function create()
    {
        $vehicleTypes = VehicleType::all();
        $drivers = Driver::all();
//        $licences = Licence::all();
        return new VehicleTypeResource([
            'vehicleTypes'  =>  $vehicleTypes,
            'drivers'   =>  $drivers,
//            'licences'  =>  $licences,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     * @param VehicleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleStoreRequest $request)
    {
        $vehicle = Vehicle::create($request->all());
        return (new VehicleResource($vehicle))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Vehicle $vehicle
     * @return VehicleResource
     */
    public function show(Vehicle $vehicle)
    {
        return new VehicleResource($vehicle);
    }

    /**
     * Update the specified resource in storage.
     * @param VehicleUpdateRequest $request
     * @param Vehicle $vehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VehicleUpdateRequest $request, Vehicle $vehicle)
    {
        $vehicle->update($request->all());

        return (new VehicleResource($vehicle))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Vehicle $vehicle
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
