<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\PayeeStoreRequest;
use App\Http\Requests\PayeeUpdateRequest;
use App\Http\Resources\PayeeResource;
use App\Models\Payee;
use Symfony\Component\HttpFoundation\Response;

class PayeeApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new PayeeResource(Payee::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param PayeeStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PayeeStoreRequest $request)
    {
        $payee = Payee::create($request->all());
        return (new PayeeResource($payee))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Payee $payee
     * @return PayeeResource
     */
    public function show(Payee $payee)
    {
        return new PayeeResource($payee);
    }

    /**
     * Update the specified resource in storage.
     * @param PayeeUpdateRequest $request
     * @param Payee $payee
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PayeeUpdateRequest $request, Payee $payee)
    {
        $payee->update($request->all());

        return (new PayeeResource($payee))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Payee $payee
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Payee $payee)
    {
        $payee->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
