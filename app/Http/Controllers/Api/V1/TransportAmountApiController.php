<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\TransportAmountStoreRequest;
use App\Http\Requests\TransportAmountUpdateRequest;
use App\Http\Resources\TransportAmountResource;
use App\Models\TransportAmount;
use Symfony\Component\HttpFoundation\Response;

class TransportAmountApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new TransportAmountResource(TransportAmount::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param TransportAmountStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TransportAmountStoreRequest $request)
    {
        $transport_amount = TransportAmount::create($request->all());
        return (new TransportAmountResource($transport_amount))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param TransportAmount $transport_amount
     * @return TransportAmountResource
     */
    public function show(TransportAmount $transport_amount)
    {
        return new TransportAmountResource($transport_amount);
    }

    /**
     * Update the specified resource in storage.
     * @param TransportAmountUpdateRequest $request
     * @param TransportAmount $transport_amount
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TransportAmountUpdateRequest $request, TransportAmount $transport_amount)
    {
        $transport_amount->update($request->all());

        return (new TransportAmountResource($transport_amount))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param TransportAmount $transport_amount
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(TransportAmount $transport_amount)
    {
        $transport_amount->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
