<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\ExcursionVehicleStoreRequest;
use App\Http\Requests\ExcursionVehicleUpdateRequest;
use App\Http\Resources\ExcursionVehicleResource;
//use App\Models\ExcursionVehicle;
use Symfony\Component\HttpFoundation\Response;

class ExcursionVehicleApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new ExcursionVehicleResource(ExcursionVehicle::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param ExcursionVehicleStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExcursionVehicleStoreRequest $request)
    {
        $excursion_vehicle = ExcursionVehicle::create($request->all());
        return (new ExcursionVehicleResource($excursion_vehicle))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param ExcursionVehicle $excursion_vehicle
     * @return ExcursionVehicleResource
     */
    public function show(ExcursionVehicle $excursion_vehicle)
    {
        return new ExcursionVehicleResource($excursion_vehicle);
    }

    /**
     * Update the specified resource in storage.
     * @param ExcursionVehicleUpdateRequest $request
     * @param ExcursionVehicle $excursion_vehicle
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ExcursionVehicleUpdateRequest $request, ExcursionVehicle $excursion_vehicle)
    {
        $excursion_vehicle->update($request->all());

        return (new ExcursionVehicleResource($excursion_vehicle))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param ExcursionVehicle $excursion_vehicle
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(ExcursionVehicle $excursion_vehicle)
    {
        $excursion_vehicle->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
