<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\ScooterPriceUpdateRequest;
use App\Http\Resources\ScooterPriceCollection;
use App\Http\Resources\ScooterPriceResource;
use App\Models\ScooterPrice;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ScooterPriceApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return ScooterPriceCollection
     */
    public function index()
    {
        $price = ScooterPrice::first();
//        dd($price->scooter->name);
        return new ScooterPriceCollection(ScooterPrice::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ScooterPrice  $scooterPrice
     * @return \Illuminate\Http\Response
     */
    public function show(ScooterPrice $scooterPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ScooterPrice  $scooterPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(ScooterPrice $scooterPrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ScooterPriceUpdateRequest $request
     * @param \App\Models\ScooterPrice $scooterPrice
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ScooterPriceUpdateRequest $request, ScooterPrice $scooterPrice)
    {
        $scooterPrice->update($request->all());

        return (new ScooterPriceResource($scooterPrice))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ScooterPrice  $scooterPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(ScooterPrice $scooterPrice)
    {
        //
    }
}
