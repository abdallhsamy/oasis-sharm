<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\Transport;
use App\Models\VehicleType;
use App\TransportDetail;
use Illuminate\Http\Request;
use App\Models\VehicleConfig;
use App\Models\VehicleTripType;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\TransportResource;
use App\Http\Requests\TransportStoreRequest;
use App\Http\Requests\TransportUpdateRequest;
use Symfony\Component\HttpFoundation\Response;

class TransportApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new TransportResource(Transport::all());
    }

    public function create()
    {
        return [
            'vehicles' => Vehicle::with(['vehicleType' => function ($q) {
                $q->with('vehicleTripTypes');
            }])->get(),
            'vehicle_types' => VehicleType::with('vehicleTripTypes')->get(),
            'vehicle_configs' => VehicleConfig::all(),
            'drivers' => Driver::all(),
            'vehicle_trip_types' => VehicleTripType::with('vehicleTypes')->get(),
        ];

    }

    public function find(Request $request)
    {
        return [
            'vehicle_trip_types' => VehicleTripType::with('vehicleTypes')->whereHas('vehicleTypes', function ($q) use($request) {
                $q->where('vehicle_type_id', $request->id);
            })->get(),
        ];

    }

    /**
     * Store a newly created resource in storage.
     * @param TransportStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TransportStoreRequest $request)
    {
        dd($request->all());
        $transport = Transport::create((is_array($request['details'])) ? Arr::except($request->all(), 'details')  : $request->all());

        if (is_array($request->details)) {
            foreach ($request->details as $detail) {
                $detail['transport_id'] = $transport->id;
                $detail['price'] = DB::table('vehicle_configs')->where('vehicle_trip_type_id', $detail['vehicle_trip_type_id'])->where('vehicle_type_id', $transport->vehicle->type_id)->first()->price;
                TransportDetail::create($detail);
            }
        }

        $transport->update([
            'commission' => $transport->calculatCommission()
        ]);

        return (new TransportResource($transport))->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param Transport $transport
     * @return TransportResource
     */
    public function show(Transport $transport)
    {
        return new TransportResource($transport);
    }

    /**
     * Update the specified resource in storage.
     * @param TransportUpdateRequest $request
     * @param Transport $transport
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TransportUpdateRequest $request, Transport $transport)
    {
        $transport->update($request->all());

        return (new TransportResource($transport))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param Transport $transport
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(Transport $transport)
    {
        $transport->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
