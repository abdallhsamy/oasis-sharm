<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\RoleCollection;
use App\Http\Resources\UserCollection;
use App\User;
use Illuminate\Http\Response;
use App\Http\Resources\UserResource;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return new UserCollection(User::all());
    }

    public function create()
    {
        return new RoleCollection(Role::all());
    }

    /**
     * Store a newly created resource in storage.
     * @param UserStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserStoreRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            "password" => Hash::make($request->password)
         ]);

        if ($request->role) $user->syncRoles([$request->role]);
        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     * @param UserUpdateRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->all());
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);
        if ($request->password)  $user->update(["password" => Hash::make($request->password)]);

        if ($request->role) $user->syncRoles([$request->role]);

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    /**
     * Remove the specified resource from storage.
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {
        foreach ($user->roles as $role) {
            $user->removeRole($role);
        }
        $user->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
