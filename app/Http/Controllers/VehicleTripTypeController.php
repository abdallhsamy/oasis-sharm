<?php

namespace App\Http\Controllers;

use App\Models\VehicleTripType;
use Illuminate\Http\Request;

class VehicleTripTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VehicleTripType  $vehicleTripType
     * @return \Illuminate\Http\Response
     */
    public function show(VehicleTripType $vehicleTripType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VehicleTripType  $vehicleTripType
     * @return \Illuminate\Http\Response
     */
    public function edit(VehicleTripType $vehicleTripType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VehicleTripType  $vehicleTripType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VehicleTripType $vehicleTripType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VehicleTripType  $vehicleTripType
     * @return \Illuminate\Http\Response
     */
    public function destroy(VehicleTripType $vehicleTripType)
    {
        //
    }
}
