<?php

namespace App\Http\Controllers;

use App\TransportDetail;
use Illuminate\Http\Request;

class TransportDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TransportDetail  $transportDetail
     * @return \Illuminate\Http\Response
     */
    public function show(TransportDetail $transportDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TransportDetail  $transportDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(TransportDetail $transportDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TransportDetail  $transportDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransportDetail $transportDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TransportDetail  $transportDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransportDetail $transportDetail)
    {
        //
    }
}
