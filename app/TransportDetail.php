<?php

namespace App;

use App\Models\Transport;
use Illuminate\Database\Eloquent\Model;

class TransportDetail extends Model
{
    protected $fillable = [
        'transport_id',
//        'vehicle_type_id',
        'vehicle_trip_type_id',
        'count',
        'price'
    ];

    public function transport()
    {
        return $this->belongsTo(Transport::class, 'transport_id');
    }

    public function vehicleTripType()
    {
        return $this->belongsTo(VehicleTripType::class, 'vehicle_trip_type_id');
    }

//    public function vehicleType()
//    {
//        return $this->belongsTo(VehicleType::class, 'vehicle_type_id');
//    }


//            if ($detail->vehicle_trip_type_id = $E_id) {
////                $E = السعر * النوع;
////                $price = VehicleConfig::where('vehicle_type_id',$detail->vehicle_type_id)
////                    ->where('vehicle_trip_type_id', $detail->vehicle_trip_type_id)
////                    ->price;
//                $letter = $detail->price * $detail->price;
//            }


}
