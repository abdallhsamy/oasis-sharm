import store from '@/store/store';

export default {
    // to get default role name of system
    getDefaultRole(role) {
        return store.getters.getDefaultRole(role);
    },

    // to check role of authenticated user
    hasRole(role) {
        return store.getters.hasRole(this.getDefaultRole(role));
    },

    // to check any permission for authenticated user
    hasAnyRole(roles) {
        return store.getters.hasAnyRole(roles);
    },

    // to check any permission for authenticated user
    hasNotAnyRole(roles) {
        return store.getters.hasNotAnyRole(roles);
    },

    // to check permission for authenticated user
    hasPermission(permission) {
        return store.getters.hasPermission(permission);
    },

    // to check any permission for authenticated user
    hasAnyPermission(permissions) {
        return store.getters.hasAnyPermission(permissions);
    },

    // to check Admin role
    hasAdminRole() {
        if (this.hasRole('admin'))
            return 1;
        else
            return 0;
    },

    // to check whether a given user has given role
    userHasRole(user, role_name) {
        if (!user.roles)
            return false;

        let user_role = user.roles.filter(role => role.name === this.getDefaultRole(role_name));
        if (user_role.length)
            return true;
        return false;
    }
};