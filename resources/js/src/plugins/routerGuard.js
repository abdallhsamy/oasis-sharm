import router from "@/router";
import store from "@/store/store";

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.auth.isUserLoggedIn()) {
            next({
                name: "login"
            });
        } else {
            next();
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        if (store.state.auth.isUserLoggedIn()) {
            next({
                name: "dashboard-analytics"
            });
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});
