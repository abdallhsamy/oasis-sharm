import Vue from "vue";
import { ValidationObserver, ValidationProvider, extend, localize, configure } from 'vee-validate';
import en from 'vee-validate/dist/locale/en.json';
import * as rules from 'vee-validate/dist/rules';


Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});

configure({
    classes: {
        // valid: 'focus:border-success-100 border-success-100',
        // invalid: 'is-invalid border-error-100 focus:border-error-100',
        // dirty: ['is-dirty', 'is-dirty'], // multiple classes per flag!÷
        // ...
    }
})
localize('en', en);

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
