// import Vue from "vue"
// import { AclInstaller, AclCreate, AclRule } from "vue-acl"
// import router from "@/router"
// import axios from '@/axios'

// Vue.use(AclInstaller)

// let initialRole = "admin"

// let userInfo = JSON.parse(localStorage.getItem("userInfo"))
// if (userInfo && userInfo.userRole) initialRole = userInfo.userRole

// let globalRules = {}

// axios.get('/global-roles').then((response) => {
//     response.data.forEach((role) => {
//         let globalRules = new AclRule(role.name).generate();
//     });
// });
// console.log(new AclRule('admin').generate())


// export default new AclCreate({
//     initial: initialRole,
//     notfound: "/pages/not-authorized",
//     router,
//     acceptLocalRules: true,
//     globalRules: {
//         admin: new AclRule('admin').generate(),
//         editor: new AclRule('editor', 'admin').generate()
//     },
//     middleware: async acl => {
//         console.log('hi')
//         await axios.get('/global-roles').then((response) => {
//             response.data.forEach((role) => {
//                 new AclRule(role.name).generate();
//                 acl.change(role.name)
//             });
//         });
//     }
// })

import Vue from "vue"
import { AclInstaller, AclCreate, AclRule } from "vue-acl"
import router from "@/router"

Vue.use(AclInstaller)

let initialRole = "admin"

let userInfo = JSON.parse(localStorage.getItem("userInfo"))
if (userInfo && userInfo.userRole) initialRole = userInfo.userRole

export default new AclCreate({
    initial: initialRole,
    notfound: "/pages/not-authorized",
    router,
    acceptLocalRules: true,
    globalRules: {
        admin: new AclRule("admin").generate(),
        editor: new AclRule("editor").or("admin").generate(),
        public: new AclRule("public").or("admin").or("editor").generate(),
    }
})