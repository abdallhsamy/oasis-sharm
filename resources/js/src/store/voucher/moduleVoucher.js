/*=========================================================================================
  File Name: moduleVoucher.js
  Description: Voucher Module
==========================================================================================*/


import state from './moduleVoucherState.js'
import mutations from './moduleVoucherMutations.js'
import actions from './moduleVoucherActions.js'
import getters from './moduleVoucherGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

