/*=========================================================================================
  File Name: moduleBankAccount.js
  Description: BankAccount Module
==========================================================================================*/


import state from './moduleBankAccountState.js'
import mutations from './moduleBankAccountMutations.js'
import actions from './moduleBankAccountActions.js'
import getters from './moduleBankAccountGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

