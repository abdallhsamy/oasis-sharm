/*=========================================================================================
  File Name: moduleVehicleType.js
  Description: VehicleType Module
==========================================================================================*/


import state from './moduleVehicleTypeState.js'
import mutations from './moduleVehicleTypeMutations.js'
import actions from './moduleVehicleTypeActions.js'
import getters from './moduleVehicleTypeGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

