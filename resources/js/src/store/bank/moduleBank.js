/*=========================================================================================
  File Name: moduleBank.js
  Description: Bank Module
==========================================================================================*/


import state from './moduleBankState.js'
import mutations from './moduleBankMutations.js'
import actions from './moduleBankActions.js'
import getters from './moduleBankGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

