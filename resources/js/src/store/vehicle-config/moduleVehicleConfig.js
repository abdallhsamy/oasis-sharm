/*=========================================================================================
  File Name: moduleVehicleConfig.js
  Description: VehicleConfig Module
==========================================================================================*/


import state from './moduleVehicleConfigState.js'
import mutations from './moduleVehicleConfigMutations.js'
import actions from './moduleVehicleConfigActions.js'
import getters from './moduleVehicleConfigGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

