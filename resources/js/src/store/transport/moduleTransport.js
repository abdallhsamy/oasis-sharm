/*=========================================================================================
  File Name: moduleTransport.js
  Description: Transport Module
==========================================================================================*/


import state from './moduleTransportState.js'
import mutations from './moduleTransportMutations.js'
import actions from './moduleTransportActions.js'
import getters from './moduleTransportGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

