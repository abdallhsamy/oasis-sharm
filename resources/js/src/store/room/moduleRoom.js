/*=========================================================================================
  File Name: moduleRoom.js
  Description: Room Module
==========================================================================================*/


import state from './moduleRoomState.js'
import mutations from './moduleRoomMutations.js'
import actions from './moduleRoomActions.js'
import getters from './moduleRoomGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

