/*=========================================================================================
  File Name: moduleDriver.js
  Description: Driver Module
==========================================================================================*/


import state from './moduleDriverState.js'
import mutations from './moduleDriverMutations.js'
import actions from './moduleDriverActions.js'
import getters from './moduleDriverGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

