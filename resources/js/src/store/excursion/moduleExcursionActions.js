/*=========================================================================================
  File Name: moduleExcursionActions.js
  Description: Excursion Module Actions
==========================================================================================*/

import axios from "@/axios.js"

export default {
    addItem({ commit }, item) {
        return new Promise((resolve, reject) => {
            axios.post("excursions", item)
                .then((response) => {
                    commit('ADD_ITEM', Object.assign(item, { id: response.data.data.id }))
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    createExcursionItems({ commit }) {
      return new Promise((resolve, reject) => {
        axios.get("excursions/create")
          .then((response) => {
              //  console.log(response.data)
            commit('SET_CREATE_EXCURSION', response.data)
            resolve(response)
          })
          .catch((error) => { reject(error) })
      })
    },
    fetchDataListItems({ commit }) {
        return new Promise((resolve, reject) => {
            axios.get("excursions")
                .then((response) => {
                    commit('SET_PRODUCTS', response.data.data)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },

    show({ commit }, payload) {
        return new Promise((resolve, reject) => {
            axios.get(`excursions/${payload}`)
                .then((response) => {
                    commit('SET_PRODUCT', response.data.data)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    // fetchEventLabels({ commit }) {
    //   return new Promise((resolve, reject) => {
    //     axios.get("/api/apps/calendar/labels")
    //       .then((response) => {
    //         commit('SET_LABELS', response.data)
    //         resolve(response)
    //       })
    //       .catch((error) => { reject(error) })
    //   })
    // },
    updateItem({ commit }, item) {
        return new Promise((resolve, reject) => {
            axios.put(`/excursions/${item.id}`, item)
                .then((response) => {
                    console.log(response.data.data)
                    commit('UPDATE_PRODUCT', response.data.data)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    removeItem({ commit }, itemId) {
        return new Promise((resolve, reject) => {
            axios.delete(`/excursions/${itemId}`)
                .then((response) => {
                    commit('REMOVE_ITEM', itemId)
                    resolve(response)
                })
                .catch((error) => { reject(error) })
        })
    },
    // eventDragged({ commit }, payload) {
    //   return new Promise((resolve, reject) => {
    //     axios.post(`/api/apps/calendar/event/dragged/${payload.event.id}`, {payload: payload})
    //       .then((response) => {

    //         // Convert Date String to Date Object
    //         let event = response.data
    //         event.startDate = new Date(event.startDate)
    //         event.endDate = new Date(event.endDate)

    //         commit('UPDATE_EVENT', event)
    //         resolve(response)
    //       })
    //       .catch((error) => { reject(error) })
    //   })
    // },
}