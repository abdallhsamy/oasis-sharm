/*=========================================================================================
  File Name: moduleCompany.js
  Description: Company Module
==========================================================================================*/


import state from './moduleExcursionState.js'
import mutations from './moduleExcursionMutations.js'
import actions from './moduleExcursionActions.js'
import getters from './moduleExcursionGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

