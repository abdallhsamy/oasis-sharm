/*=========================================================================================
  File Name: moduleTrip.js
  Description: Trip Module
==========================================================================================*/


import state from './moduleTripState.js'
import mutations from './moduleTripMutations.js'
import actions from './moduleTripActions.js'
import getters from './moduleTripGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

