/*=========================================================================================
  File Name: moduleAuthState.js
  Description: Auth Module State
  ==========================================================================================*/


import auth from "@/auth/authService";

export default {
    isUserLoggedIn: () => {
        let isAuthenticated = true;

        return (localStorage.getItem('userInfo') && localStorage.getItem('accessToken'));
    },
};
