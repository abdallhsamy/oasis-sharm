/*=========================================================================================
  File Name: moduleAuthMutations.js
  Description: Auth Module Mutations
  ==========================================================================================*/

import axios from "../../http/axios/index.js"
import router from '@/router'

export default {
    SET_BEARER(state, accessToken) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('accessToken')
        
    }
}
