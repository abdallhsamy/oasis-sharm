/*=========================================================================================
  File Name: modulePayee.js
  Description: Payee Module
==========================================================================================*/


import state from './modulePayeeState.js'
import mutations from './modulePayeeMutations.js'
import actions from './modulePayeeActions.js'
import getters from './modulePayeeGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

