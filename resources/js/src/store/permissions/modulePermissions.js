/*=========================================================================================
  File Name: modulePermission.js
  Description: Permission Module
==========================================================================================*/


import state from './modulePermissionsState.js'
import mutations from './modulePermissionsMutations.js'
import actions from './modulePermissionsActions.js'
import getters from './modulePermissionsGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

