/*=========================================================================================
  File Name: getters.js
  Description: Vuex Store - getters
  ==========================================================================================*/

// added so later we can keep breakpoint in sync automatically using this config file
// import tailwindConfig from "../../tailwind.config.js"

const getters = {

    // COMPONENT
    // vx-autosuggest
    // starredPages: state => state.navbarSearchAndPinList.data.filter((page) => page.highlightAction),
    windowBreakPoint: state => {

        // This should be same as tailwind. So, it stays in sync with tailwind utility classes
        if (state.windowWidth >= 1200) return "xl";
        else if (state.windowWidth >= 992) return "lg";
        else if (state.windowWidth >= 768) return "md";
        else if (state.windowWidth >= 576) return "sm";
        else return "xs";
    },
    hasRole: (state) => (name) => {
        return (state.AppActiveUser.roles.indexOf(name) >= 0) ? true : false;
    },
    hasAnyRole: (state) => (roles) => {
        return (state.AppActiveUser.roles.some(role => {
            return roles.indexOf(role) > -1;
        })) ? true : false;
    },
    hasNotAnyRole: (state) => (roles) => {
        return (state.AppActiveUser.roles.every(role => {
            return roles.indexOf(role) < 0;
        })) ? true : false;
    },
    hasPermission: (state) => (name) => {
        if (state.AppActiveUser) {
            return (state.AppActiveUser.permissions.indexOf(name) > -1) ? true : false;
        }
    },
    hasAnyPermission: (state) => (permissions) => {
        return (state.AppActiveUser.permissions.some(permission => {
            return permissions.indexOf(permission) > -1;
        })) ? true : false;
    },
    getDefaultRole: (state) => (name) => {
        return state.config.default_roles ? state.config.default_roles[name] : '';
    },
};

export default getters;
