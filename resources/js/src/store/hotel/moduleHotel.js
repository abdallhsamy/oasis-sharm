/*=========================================================================================
  File Name: moduleHotel.js
  Description: Hotel Module
==========================================================================================*/


import state from './moduleHotelState.js'
import mutations from './moduleHotelMutations.js'
import actions from './moduleHotelActions.js'
import getters from './moduleHotelGetters.js'

export default {
  isRegistered: false,
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

