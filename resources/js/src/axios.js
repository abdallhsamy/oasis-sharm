// axios
import axios from 'axios'

const domain = "http://oasissharm.test/api/v1/"

axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('accessToken')

export default axios.create({
  baseURL: domain
  // You can add your headers here
})
